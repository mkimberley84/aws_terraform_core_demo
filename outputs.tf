output "primary_subnets_map" {
    value = ["${module.generic_vpc.primary_subnet_map}"]
}
output "secondary_subnets_map" {
    value = "${module.generic_vpc.secondary_subnet_map}"
}
