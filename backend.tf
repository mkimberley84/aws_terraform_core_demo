terraform {
    backend "s3" {	
    bucket = "mkimberleytfstate"
    key    = "sandbox/core/terraform.tfstate"
    region = "eu-west-2"
    }
}
