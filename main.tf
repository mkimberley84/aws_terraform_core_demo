provider "aws" {
    region = "eu-west-2"
}


module "generic_vpc" {
#    source = "git@github.com:DarkIn5anity/aws_terraform_mod_common.git//network/vpc"
    source                               = "git::ssh://git@github.com/DarkIn5anity/aws_terraform_mod_common//network/vpc"
    vpc_environment                      = "${var.environment}"
    vpc_name                             = "${var.vpc_name}"
    vpc_cidr                             = "${var.vpc_cidr}"
    vpc_tags                             = "${var.vpc_tags}"
    vpc_instance_tenancy                 = "${var.vpc_instance_tenancy}"
    vpc_enable_dns_hostnames             = "${var.vpc_enable_dns_hostnames}"
    vpc_enable_dns_support               = "${var.vpc_enable_dns_support}"
    vpc_primary_cidr                     = "${var.vpc_primary_subnet}"
    vpc_secondary_cidr                   = "${var.vpc_secondary_subnet}"
    vpc_primary_subnet_name              = "${var.vpc_primary_subnet_name}"
    vpc_secondary_subnet_name            = "${var.vpc_secondary_subnet_name}"
    vpc_primary_public_ip_on_lauch       = "${var.vpc_primary_public_ip_on_lauch}"
    vpc_secondary_public_ip_on_lauch     = "${var.vpc_secondary_public_ip_on_lauch}"
}