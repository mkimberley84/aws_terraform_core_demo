environment                         = "Sandbox"
vpc_name                            = "MK VPC"
vpc_enable_dns_hostnames            = true
vpc_enable_dns_support              = true
vpc_cidr                            = "10.70.0.0/16"
vpc_primary_subnet                  = "10.70.1.0/24"
vpc_secondary_subnet                = "10.70.2.0/24"
vpc_primary_subnet_name             = "MK Public"
vpc_secondary_subnet_name           = "MK Private"
vpc_instance_tenancy                = "default"
vpc_primary_public_ip_on_lauch      = true
vpc_secondary_public_ip_on_lauch    = false

vpc_tags                 = {     
    Name = "MKimberley VPC"
    Description = "MKimberley Test VPC"
}
